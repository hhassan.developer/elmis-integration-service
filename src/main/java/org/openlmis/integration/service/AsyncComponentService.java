package org.openlmis.integration.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.databind.json.JsonMapper;
import java.io.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import java.util.concurrent.TimeUnit;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.json.JSONObject;
import org.openlmis.integration.dto.dhis.DataSetDto;
import org.openlmis.integration.dto.dhis.DataSetElement;
import org.openlmis.integration.dto.dhis.DhisMessage;
import org.openlmis.integration.dto.dhis.ElmisInterfaceDataSetDto;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.openlmis.integration.dto.*;
import org.openlmis.integration.dto.dhis.ElmisInterfaceDto;
import org.openlmis.integration.dto.dhis.ProcessingPeriod;
import org.openlmis.integration.dto.dhis.ZoneDto;
import org.openlmis.integration.mapper.InterfaceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

@Component("AsyncComponentService")
public class AsyncComponentService {

  // public static final String BASE_HIM_URL = "https://him-dev.moh.go.tz:5000";
  public static final String BASE_HIM_URL = "https://him.moh.go.tz:5000";
  // private static final String HIM_USERNAME = "elmis_client";
  // private static final String HIM_PASSWORD = "himelmis2021";

  private static final String HIM_USERNAME = "elmis-client";
  private static final String HIM_PASSWORD = "$H1mElm1s2021.";

  //DHIS2
  public static final String DHIS2_URL = "https://him.moh.go.tz:5000/elmis-dhis2";
  public static final String DHIS2_URL1 = "https://41.59.227.95:5000/elmis-dhis2";
  private static final String DHIS2_USERNAME = "elmis-client";
  private static final String DHIS2_PASSWORD = "$H1mElm1s2021.";

  public static final String DHIS2_URL_TEST = "https://integrations.dhis2.udsm.ac.tz/api/dataValueSets?orgUnitIdScheme=code";
  private static final String DHIS2_USERNAME_TEST = "elmis";
  private static final String DHIS2_PASSWORD_TEST = "Cozzy@123";

  private static final String NUMBER_OF_INCIDENCIES = "SAlxgS3I4Xi";
  private static final String TOTAL_EXPECTED_INCIDENCIES = "cAFRtErVM2F";

  private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

  @Autowired
  private InterfaceMapper repository;

  @Async("asyncExecutor")
  public void asyncEmergencyCommodities() {
    System.out.println("Execute EmergencyCommodities. " + Thread.currentThread().getName());
    processEmergencyCommoditiesData();
  }

  @Async("asyncExecutor")
  public void asyncForecastingData() {
    System.out.println("Execute Forecasting. ");
    processForeCastData();
  }

  @Async("asyncExecutor")
  public void asyncPercentageRequisitionData() {
    System.out.println("Execute Percentage Requisition. ");
    processPercentageRejected();
  }

  @Async("asyncExecutor")
  public void asyncProcessTimelinessReporting() {
    System.out.println("Execute  Timeliness reporting. ");
    processTimelinessReporting();
  }

  @Async("asyncExecutor")
  public void asyncProcessTurnAroundTime() {
    System.out.println("Execute Order Turn around time. ");
    processTurnAroundTime();
  }

  @Async("asyncExecutor")
  public void asyncProcessStockAvailability() {
    System.out.println("Execute Stock availability. ");
    processStockAvailability();
  }

  @Async("asyncExecutor")
  public void asyncProcessProductList() {
    System.out.println("Execute Product List. ");
    processProductList();
  }

  @Async("asyncExecutor")
  public void asyncProcessStockOnHand() {
    System.out.println("Execute Stcok On Hand. ");
    processStockOnHand();
  }

  @Async("asyncExecutor")
  public void asyncProcessPercentageWastage() {
    System.out.println("Execute Wastage. ");
    processPercentageWastage();
  }

  @Async("asyncExecutor")
  public void asyncProcessPrograms() {
    System.out.println("Execute Programs. ");
    processPrograms();
  }

  public void processEmergencyCommoditiesData() {

    String url = BASE_HIM_URL + "/thscp-emergency-commodity-stock-status";

    for (ProcessingPeriod period : getCurrentPeriod()) {

      List<EmergencyCommodity> periods = repository.getThScpEmergencyCommodities(period.getId());
      if (!periods.isEmpty()) {
        for (List<EmergencyCommodity> periodList : Lists.partition(periods, 100)) {
          sendPostApiData(HIM_USERNAME, HIM_PASSWORD, url, periodList, null, null,
              null, null, null, null, null, null, null);
        }

      } else System.out.println("empty period");
    }
  }

  private void processForeCastData() {

    String url = BASE_HIM_URL + "/thscp-forecast-accuracy-per-program";
    for (ProcessingPeriod period : getCurrentPeriod()) {

      List<Forecasting> forecastingList = repository.getForeCastedData(period.getId());

      if (!forecastingList.isEmpty()) {
        for (List<Forecasting> forecastingList1 : Lists.partition(forecastingList, 100)) {

          sendPostApiData(HIM_USERNAME, HIM_PASSWORD, url, null, forecastingList1, null,
              null, null, null, null, null, null, null);
        }
      }
    }
  }

  private void processPercentageRejected() {

    String url = BASE_HIM_URL + "/thscp-percentage-of-reports-and-requisitions-rejected";

    for (ProcessingPeriod period : getCurrentPeriod()) {

      List<PercentageRejectedRequisition> percentageRejectedRequisitionList = repository.getPercentageRejected(period.getId());

      if (!percentageRejectedRequisitionList.isEmpty()) {

        for (List<PercentageRejectedRequisition> rejectedRequisitionList :
            Lists.partition(percentageRejectedRequisitionList, 100)) {
          sendPostApiData(HIM_USERNAME, HIM_PASSWORD, url, null, null, rejectedRequisitionList, null,
              null, null, null, null, null, null);
        }

      }
    }
  }

  private void processTimelinessReporting() {

    String url = BASE_HIM_URL + "/thscp-reporting-timeliness";

    for (ProcessingPeriod period : getCurrentPeriod()) {

      List<TimelinessReporting> timelinessReportings = repository.getTimelinessReporting(period.getId());

      if (!timelinessReportings.isEmpty()) {
        System.out.println("Is not empty");
        for (List<TimelinessReporting> reportingList : Lists.partition(timelinessReportings, 100)) {

          for (List<TimelinessReporting> reportings : Lists.partition(reportingList, 100)) {

            sendPostApiData(HIM_USERNAME, HIM_PASSWORD, url, null, null, null, reportings,
                null, null, null, null, null, null);
          }
        }
      }
    }
  }

  private void processTurnAroundTime() {

    String url = BASE_HIM_URL + "/thscp-turn-around-time-orchestrator";
    for (ProcessingPeriod period : getCurrentPeriod()) {

      List<TurnAroundTime> turnAroundTime = repository.getTurnAroundTime(period.getId());
      if (!turnAroundTime.isEmpty()) {
        for (List<TurnAroundTime> turnAroundTimes : Lists.partition(turnAroundTime, 100)) {

          sendPostApiData(HIM_USERNAME, HIM_PASSWORD, url, null, null, null,
              null, turnAroundTimes, null, null, null, null, null);
        }
      }

    }
  }

  private void processStockAvailability() {

    String url = BASE_HIM_URL + "/thscp-stock-availability";
    for (ProcessingPeriod period : getCurrentPeriod()) {
      List<StockAvailability> stockAvailabilities = repository.getStockAvailability(period.getId());
      if (!stockAvailabilities.isEmpty()) {
        for (List<StockAvailability> availabilityList : Lists.partition(stockAvailabilities, 100)) {
          sendPostApiData(HIM_USERNAME, HIM_PASSWORD, url, null, null, null, null,
              null, availabilityList, null, null, null, null);
        }
      }
    }
  }

  private void processProductList() {

    String url = BASE_HIM_URL + "/thscp-product-list";

    List<Product> productList = repository.getProductList();
    if (!productList.isEmpty()) {
      for (List<Product> products : Lists.partition(productList, 100)) {
        sendPostApiData(HIM_USERNAME, HIM_PASSWORD, url, null, null, null, null,
            null, null, products, null, null, null);
      }
    }
  }

  private void processStockOnHand() {

    String url = BASE_HIM_URL + "/elmis-thscp-stock-on-hand";

    for (ProcessingPeriod period : getCurrentPeriod()) {

      List<StockOnHand> stockOnHands = repository.getStockOnHand(period.getId());

      if (!stockOnHands.isEmpty()) {
        for (List<StockOnHand> stockOnHandList : Lists.partition(stockOnHands, 100)) {
          sendPostApiData(HIM_USERNAME, HIM_PASSWORD, url, null, null, null, null,
              null, null, null, stockOnHandList, null, null);
        }
      } else
        System.out.println("Empty soh");
    }
  }

  private void processPercentageWastage() {

    String url = BASE_HIM_URL + "/elmis-thscp-percentage-of-wastage";
    for (ProcessingPeriod period : getCurrentPeriod()) {

      List<PercentageWastage> stockOnHands = repository.getPercentageWastage(period.getId());

      if (!stockOnHands.isEmpty()) {
        for (List<PercentageWastage> wastageList : Lists.partition(stockOnHands, 100)) {
          sendPostApiData(HIM_USERNAME, HIM_PASSWORD, url, null, null, null, null,
              null, null, null, null, null, wastageList);
        }
      }
    }
  }

  private void processPrograms() {
    String url = BASE_HIM_URL + "/thscp-program-list";

    List<Program> programs = repository.getAllPrograms();

    sendPostApiData(HIM_USERNAME, HIM_PASSWORD, url, null, null, null, null,
        null, null, null, null, programs, null);
  }

  //Function to post data dynamically
  private void sendPostApiData(String username, String password, String url, List<EmergencyCommodity> data,
                               List<Forecasting> forecastingList, List<PercentageRejectedRequisition> rejectedRequisitions,
                               List<TimelinessReporting> timelinessReportings, List<TurnAroundTime> turnAroundTimes, List<StockAvailability> stockAvailabilities,
                               List<Product> productList, List<StockOnHand> stockOnHands, List<Program> programs,
                               List<PercentageWastage> wastages) {
    ObjectMapper mapper = new ObjectMapper();
    java.net.URL obj = null;
    try {
      obj = new URL(url);

      System.out.println("is HTTP");
      HttpURLConnection con = (HttpURLConnection) obj.openConnection();

      String jsonInString = "";
      if (data != null) {
        jsonInString = mapper.writeValueAsString(data);
      } else if (forecastingList != null) {
        jsonInString = mapper.writeValueAsString(forecastingList);
      } else if (rejectedRequisitions != null) {
        jsonInString = mapper.writeValueAsString(rejectedRequisitions);
      } else if (timelinessReportings != null) {
        jsonInString = mapper.writeValueAsString(timelinessReportings);
      } else if (turnAroundTimes != null) {
        jsonInString = mapper.writeValueAsString(turnAroundTimes);
      } else if (stockAvailabilities != null) {
        jsonInString = mapper.writeValueAsString(stockAvailabilities);
      } else if (productList != null) {
        jsonInString = mapper.writeValueAsString(productList);
      } else if (stockOnHands != null) {
        jsonInString = mapper.writeValueAsString(stockOnHands);
      } else if (programs != null) {
        jsonInString = mapper.writeValueAsString(programs);
      } else if (wastages != null) {
        jsonInString = mapper.writeValueAsString(wastages);
      }

      System.out.println(jsonInString);

      String userCredentials = username + ":" + password;
      String basicAuth = "Basic " + new String(java.util.Base64.getEncoder().encode(userCredentials.getBytes()));
      con.setRequestProperty("Authorization", basicAuth);
      con.setRequestMethod("POST");
      con.setRequestProperty("Content-Type", "application/json");
      con.setDoOutput(true);
      OutputStream wr = con.getOutputStream();
      wr.write(jsonInString.getBytes("UTF-8"));

      wr.flush();
      wr.close();

      int responseCode = con.getResponseCode();

      BufferedReader in = new BufferedReader(
          new InputStreamReader(con.getInputStream()));
      String inputLine;
      StringBuilder response = new StringBuilder();

      while ((inputLine = in.readLine()) != null) {
        response.append(inputLine);
      }
      in.close();
      System.out.println("Response from HIM");
      System.out.println(response);
      //print result

    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  private List<ProcessingPeriod> getCurrentPeriod() {
    return repository.getByStartDate();
  }

  @Async("asyncExecutor")
  public void pullFacilities() throws IOException {
    int pageSize = 50;
    int currentPage = 1;
    HealthFacility facility = null;
    boolean firstTime = false;
    int offSet = 0;

    ObjectMapper mapper = new ObjectMapper();

    facility = getHealthFacilities(pageSize, 1);

    if (facility != null) {

      for (currentPage = 1; currentPage <= facility.getCount(); currentPage++) {
        facility = getHealthFacilities(pageSize, currentPage);

        if (facility != null) {
          if (!facility.getFacilityData().isEmpty()) {

            for (HealthFacilityDTO facilityDTO : facility.getFacilityData()) {
              saveHFR(facilityDTO);
            }
          } else {
            return;
          }
        }

        System.out.println(mapper.writeValueAsString(facility));
      }
    }

  }

  private HealthFacility getHealthFacilities(Integer pageSize, Integer currentPage) throws IOException {

    if (currentPage != 0) {
      System.out.println(currentPage);

      String url = "http://hfrs.moh.go.tz/web/index.php?r=site/facility-list&reqToken=DjzZ3YaAfNfCu9TXMuHExCPhW0OMac9vZWpjZ0svna38RNW&search_query=operating&pageSize=" + pageSize + "&page=" + currentPage;

      HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
      //add headers to the connection, or check the status if desired..

      // handle error response code it occurs
      int responseCode = connection.getResponseCode();
      InputStream inputStream;
      if (200 <= responseCode && responseCode <= 299) {
        inputStream = connection.getInputStream();
      } else {
        inputStream = connection.getErrorStream();
      }

      BufferedReader in = new BufferedReader(
          new InputStreamReader(
              inputStream));

      StringBuilder response = new StringBuilder();
      String currentLine;

      while ((currentLine = in.readLine()) != null)
        response.append(currentLine);

      in.close();
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      return mapper.readValue(response.toString(), HealthFacility.class);
    }
    return null;

  }

  @Async("asyncExecutor")
  public void testPagination() {
    int pageSize = 2;
    int currentPage = 1;
    List<Program> periods;
    do {

      int offSet = (currentPage - 1) * pageSize;

      periods = repository.programs(189L, 1L, 472L, pageSize, offSet);

      String url = BASE_HIM_URL + "/thscp-program-list";

      if (!periods.isEmpty()) {

        sendPostApiData(HIM_USERNAME, HIM_PASSWORD, url, null, null, null, null,
            null, null, null, null, periods, null);
      }

     /*  ObjectMapper mapper = new ObjectMapper();
       try {
         System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(periods.size()));
         System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(periods));
       } catch (JsonProcessingException e) {
         e.printStackTrace();
       }*/
      currentPage++;

    } while (nextPage(periods, currentPage));

  }

  private boolean nextPage(List<Program> items, Integer page) {
    if (CollectionUtils.isNotEmpty(items)) {
      System.out.println("Fetched {} with {} items " + page + " and Items " + items.size());
      return true;
    } else {
      System.out.println("Returned False " + page + " and Items " + items.size());
      return false;
    }
  }

  @Async("asyncExecutor")
  public void asyncDhis2Expected() throws JsonProcessingException {
ObjectMapper mapper = new ObjectMapper();
    String username = DHIS2_USERNAME;
       // DHIS2_USERNAME_TEST;
    String password = DHIS2_PASSWORD;
        //DHIS2_PASSWORD_TEST;
    String url = DHIS2_URL;
        //DHIS2_URL_TEST;
    System.out.println(username);
    System.out.println(password);
    System.out.println(url);

    if (getCurrentPeriod() != null) {

     // for (ProgramDto program : repository.getAllPrograms2()) {
        for (ProcessingPeriod period : getCurrentPeriod()) {
          System.out.println(period.getName());
          List<ZoneDto> zoneDtos = repository.getZones();
          String year = "";
          for (ZoneDto zone : zoneDtos) {

            if (zone != null) {

              if (period.getEndDate().getMonth() >= 10) {
                  year = period.getStringYear() + period.getEndDate().getMonth();
                }  else {
                  year = period.getStringYear() + "0"+ period.getEndDate().getMonth();
              }
              System.out.println(year);

              DataSetDto object = new DataSetDto();
              String date = simpleDateFormat.format(period.getEndDate());
              object.setCompleteDate(date);
              object.setOrgUnit(zone.getHfrCode());
              object.setPeriod(Long.valueOf(year));
              object.setDataSet(NUMBER_OF_INCIDENCIES);

              List<DataSetElement> periods =
                  repository.getNumberOfExpected(period.getId(), zone.getFacilityId());

              if (!periods.isEmpty()) {
                System.out.println("period not en");

                object.setDataValues(periods);
                Log log = new Log();
                log.setJsonString(mapper.writeValueAsString(object));
                log.setSent(false);
                repository.insertLog(log);
                // sendBedNetData(username, password, url, dto, null, null, null, null, null);

              }

            }

          }

        }
     // }

    }

    System.out.println("Method one called  by Thread : " + Thread.currentThread().getName() + "  at " + new Date());

  }

  public void saveHFR(HealthFacilityDTO dto) {

    if (dto != null) {

      HealthFacilityDTO savedFacility = repository.getByFacilityCode(dto.getFacIDNumber());

      if (savedFacility == null) {
        repository.insert(dto);


      } else {
        repository.update(dto);
      }

    }

  }

  @Async
  public void methodOne() {

    String username = DHIS2_USERNAME;
    String password = DHIS2_PASSWORD;
    String url = DHIS2_URL;
    System.out.println(username);
    System.out.println(password);
    System.out.println(url);

    if (getCurrentPeriod() != null) {
      System.out.println("Users List");
      for (ProcessingPeriod period : getCurrentPeriod()) {

        List<ZoneDto> zoneDtos = repository.getZones();

        for (ZoneDto zone : zoneDtos) {
          if (zone != null) {

            List<ElmisInterfaceDataSetDto> periods = repository.getDhisNumberOfExpected(period.getId(), zone.getZoneId());
            if (!periods.isEmpty()) {
              for (List<ElmisInterfaceDataSetDto> dataSetDtoList : Lists.partition(periods, 100)) {

                if (username != null & password != null & url != null & !periods.isEmpty()) {
                  System.out.println("Users 1");
                  ElmisInterfaceDto dto = new ElmisInterfaceDto();
                  dto.setDataValues(dataSetDtoList);
                  sendBedNetData(username, password, url, dto, null);
                }
              }

            }

          }
        }

      }
    }

    System.out.println("Method one called  by Thread : " + Thread.currentThread().getName() + "  at " + new Date());
  }

  @Async
  public void methodTwo() throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    if (getCurrentPeriod() != null) {
      String year = "";
      for (ProcessingPeriod period : getCurrentPeriod()) {

        List<ZoneDto> zoneDtos = repository.getZones();

        for (ZoneDto zone : zoneDtos) {
          if (zone != null) {

            if (period.getEndDate().getMonth() >= 10) {
              year = period.getStringYear() + period.getEndDate().getMonth();
            }  else {
              year = period.getStringYear() + "0"+ period.getEndDate().getMonth();
            }

            DataSetDto object = new DataSetDto();
            String date = simpleDateFormat.format(period.getEndDate());
            object.setCompleteDate(date);
            object.setOrgUnit(zone.getHfrCode());
            object.setPeriod(Long.valueOf(year));
            object.setDataSet(TOTAL_EXPECTED_INCIDENCIES);

            List<DataSetElement> periods = repository.getAvailability(period.getId(), zone.getFacilityId());
            if (!periods.isEmpty()) {

              object.setDataValues(periods);

              Log log = new Log();
              log.setJsonString(mapper.writeValueAsString(object));
              log.setSent(false);
              repository.insertLog(log);


             /* for (List<ElmisInterfaceDataSetDto> periodList : Lists.partition(periods, 100)) {

                if (!periodList.isEmpty()) {
                  ElmisInterfaceDto dto = new ElmisInterfaceDto();
                  dto.setDataValues(periodList);
                  sendBedNetData(DHIS2_USERNAME, DHIS2_PASSWORD, DHIS2_URL, dto, null);
                }
              }*/

            }

          }
        }


      }
    }

    System.out.println("Method two called  by Thread : " + Thread.currentThread().getName() + "  at " + new Date());
  }

  @Async
  public void methodThree() throws JsonProcessingException {

    if (getCurrentPeriod() != null) {
 ObjectMapper mapper = new ObjectMapper();
    String year ="";
      for (ProcessingPeriod period : getCurrentPeriod()) {

        List<ZoneDto> zoneDtos = repository.getZones();

        for (ZoneDto zone : zoneDtos) {
          if (zone != null) {

            if (period.getEndDate().getMonth() >= 10) {
              year = period.getStringYear() + period.getEndDate().getMonth();
            }  else {
              year = period.getStringYear() + "0"+ period.getEndDate().getMonth();
            }
            System.out.println(year);

            DataSetDto object = new DataSetDto();
            String date = simpleDateFormat.format(period.getEndDate());
            object.setCompleteDate(date);
            object.setOrgUnit(zone.getHfrCode());
            object.setPeriod(Long.valueOf(year));
            object.setDataSet("fEb0XNE3yaf");

            List<DataSetElement> periods = repository.getMonthly(period.getId(), period.getScheduleId(), zone.getFacilityId());

            if (!periods.isEmpty()) {

              object.setDataValues(periods);

              Log log = new Log();
              log.setJsonString(mapper.writeValueAsString(object));
              log.setSent(false);
              repository.insertLog(log);

          /*    for (List<ElmisInterfaceDataSetDto> periodlist : Lists.partition(periods, 100)) {

                if (!periods.isEmpty()) {
                  ElmisInterfaceDto dto = new ElmisInterfaceDto();
                  dto.setDataValues(periodlist);
                  sendBedNetData(DHIS2_USERNAME, DHIS2_PASSWORD, DHIS2_URL, dto, null);

                }
              }*/

            }
          }

        }
      }

    }
    System.out.println("Method three called  by Thread : " + Thread.currentThread().getName() + "  at " + new Date());

  }

  @Async
  public void methodFour() throws JsonProcessingException {
   String year = "";
    ObjectMapper mapper = new ObjectMapper();
    if (getCurrentPeriod() != null) {

      for (ProcessingPeriod period : getCurrentPeriod()) {

        List<ZoneDto> zoneDtos = repository.getZones();

        for (ZoneDto zone : zoneDtos) {
          if (zone != null) {

            if (period.getEndDate().getMonth() >= 10) {
              year = period.getStringYear() + period.getEndDate().getMonth();
            }  else {
              year = period.getStringYear() + "0"+ period.getEndDate().getMonth();
            }
            System.out.println(year);

            DataSetDto object = new DataSetDto();
            String date = simpleDateFormat.format(period.getEndDate());
            object.setCompleteDate(date);
            object.setOrgUnit(zone.getHfrCode());
            object.setPeriod(Long.valueOf(year));
            object.setDataSet("kFY44I2xTeA");

            List<DataSetElement> periods = repository.getOrdered(period.getId(), period.getScheduleId(), zone.getFacilityId());

            if (!periods.isEmpty()) {

              object.setDataValues(periods);

              Log log = new Log();
              log.setJsonString(mapper.writeValueAsString(object));
              log.setSent(false);
              repository.insertLog(log);

             /* for (List<ElmisInterfaceDataSetDto> periodList : Lists.partition(periods, 100)) {

                ElmisInterfaceDto dto = new ElmisInterfaceDto();
                dto.setDataValues(periodList);
                sendBedNetData(DHIS2_USERNAME, DHIS2_PASSWORD, DHIS2_URL, dto, null);

              }*/
            }

          }
        }

      }
    }

    System.out.println("Method three called  by Thread : " + Thread.currentThread().getName() + "  at " + new Date());
  }

  @Async
  public void methodFive() throws JsonProcessingException {
    JsonMapper mapper = new JsonMapper();
    String year ="";
    if (getCurrentPeriod() != null) {

      for (ProcessingPeriod period : getCurrentPeriod()) {

        List<ZoneDto> zoneDtos = repository.getZones();

        for (ZoneDto zone : zoneDtos) {
          if (zone != null) {

            if (period.getEndDate().getMonth() >= 10) {
              year = period.getStringYear() + period.getEndDate().getMonth();
            }  else {
              year = period.getStringYear() + "0"+ period.getEndDate().getMonth();
            }
            System.out.println(year);

            DataSetDto object = new DataSetDto();
            String date = simpleDateFormat.format(period.getEndDate());
            object.setCompleteDate(date);
            object.setOrgUnit(zone.getHfrCode());
            object.setPeriod(Long.valueOf(year));
            object.setDataSet("LWfYOQ9fZfR");

            List<DataSetElement> periods = repository.getQuantityReceived(period.getId(), period.getScheduleId(), zone.getFacilityId());

            if (!periods.isEmpty()) {

              object.setDataValues(periods);

              Log log = new Log();
              log.setJsonString(mapper.writeValueAsString(object));
              log.setSent(false);
              repository.insertLog(log);

             /* for (List<ElmisInterfaceDataSetDto> periodList : Lists.partition(periods, 100)) {
                ElmisInterfaceDto dto = new ElmisInterfaceDto();
                dto.setDataValues(periodList);
                sendBedNetData(DHIS2_USERNAME, DHIS2_PASSWORD, DHIS2_URL, dto, null);
              }*/
            }

          }
        }

      }
    }

    System.out.println("Method three called  by Thread : " + Thread.currentThread().getName() + "  at " + new Date());
  }

  private static void disableSslVerification() {
    try {
      // Create a trust manager that does not validate certificate chains
      TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
          return null;
        }

        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }

        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
      }
      };

      // Install the all-trusting trust manager
      SSLContext sc = SSLContext.getInstance("SSL");
      sc.init(null, trustAllCerts, new java.security.SecureRandom());
      HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

      // Create all-trusting host name verifier
      HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
          return true;
        }
      };

      // Install the all-trusting host verifier
      HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (KeyManagementException e) {
      e.printStackTrace();
    }
  }

  private void sendBedNetData(String username, String password, String url, ElmisInterfaceDto data, Log dataSetDto) {
    ObjectMapper mapper = new ObjectMapper();
    java.net.URL obj = null;
    try {
      obj = new URL(url);

      System.out.println("is HTTP");
      HttpURLConnection con = (HttpURLConnection) obj.openConnection();
      //disableSslVerification();

      String jsonInString = "";

      if (data != null) {
        jsonInString = mapper.writeValueAsString(data);
      }
      if(dataSetDto != null) {
        JSONObject json = new JSONObject(dataSetDto.getJsonString());
        jsonInString = json.toString();
      }

      System.out.println(jsonInString);

      String userCredentials = username + ":" + password;
      String basicAuth = "Basic " + new String(java.util.Base64.getEncoder().encode(userCredentials.getBytes()));
      con.setRequestProperty("Authorization", basicAuth);
      con.setRequestMethod("POST");
      con.setRequestProperty("Content-Type", "application/json");
      con.setDoOutput(true);
      OutputStream wr = con.getOutputStream();
      wr.write(jsonInString.getBytes("UTF-8"));

      wr.flush();
      wr.close();

      int responseCode = con.getResponseCode();

      BufferedReader in = new BufferedReader(
          new InputStreamReader(con.getInputStream()));
      String inputLine;
      StringBuilder response = new StringBuilder();

      while ((inputLine = in.readLine()) != null) {
        response.append(inputLine);
      }
      in.close();
      System.out.println("Response from DHIS2");
      System.out.println(response);
      //print result
      //save logs

      DhisMessage message = mapper.readValue(response.toString(), DhisMessage.class);

      if(dataSetDto != null) {

        if (!message.getStatus().equalsIgnoreCase("SUCCESS")) {
          LogResponse response1 = new LogResponse();
          response1.setJsonString(jsonInString);
          response1.setErrorString(response.toString());
          repository.insertResponse(response1);
        }
          repository.delete(dataSetDto.getId());

      }

    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  @Async
  public void sendLLINData() {

    if (getCurrentPeriod() != null) {

      int endYear = Calendar.getInstance().get(Calendar.YEAR);

      for (int year = 2022; year <= endYear; year++) {

        List<ElmisInterfaceDataSetDto> periods = repository.getMosquitoNetData(Long.valueOf(year));

        for (List<ElmisInterfaceDataSetDto> periodList : Lists.partition(periods, 100)) {
          ElmisInterfaceDto dto = new ElmisInterfaceDto();
          dto.setDataValues(periodList);
          sendBedNetData(DHIS2_USERNAME, DHIS2_PASSWORD, DHIS2_URL, dto, null);
        }

      }

    }
  }

  int count = 0;
  public void sendDataFromDB() {
    System.out.println("start to run - "+ count +1);
  //  List<Log> getAll  = repository.getLogs();
    Log log2 = repository.getOnLog();
    sendBedNetData(DHIS2_USERNAME,
        DHIS2_PASSWORD, DHIS2_URL, null, log2);
    /*if(!getAll.isEmpty()) {

      try {

        for (Log l : getAll) {

          //for (Log l : log) {
          sendBedNetData(DHIS2_USERNAME,
              DHIS2_PASSWORD, DHIS2_URL, null, l);
          //  }
          System.out.println("attempted");
          TimeUnit.SECONDS.sleep(1);

        }

      }
      catch (Exception e) {
        System.out.println("Oops! Something went wrong!");
      }

    }*/
  }


}

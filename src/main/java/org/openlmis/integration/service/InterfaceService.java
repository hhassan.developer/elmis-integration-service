package org.openlmis.integration.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;

import org.apache.commons.codec.EncoderException;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.JSONObject;
import org.openlmis.integration.dto.OrderableBuilder;
import org.openlmis.integration.dto.OrderableDto;
import org.openlmis.integration.dto.ProductTest;
import org.openlmis.integration.dto.apidto.AuthParameterDto;
import org.openlmis.integration.dto.apidto.Item;
import org.openlmis.integration.dto.apidto.PCMTProduct;
import org.openlmis.integration.dto.PCMTResponseBody;
import org.openlmis.integration.dto.PMCTAccessToken;
import org.openlmis.integration.dto.User;
import org.openlmis.integration.mapper.InterfaceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.security.cert.X509Certificate;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class InterfaceService {

    private final RestOperations restTemplate = new RestTemplate();

    @Value("${pcmt_base_url}")
    private String PMCT_BASE_URL;

    @Value("${pcmt_auth_token_api}")
    private String token_api;

    @Value("${pcmt_product_api}")
    private String product_api;

    @Value("${pcmt_auth_token_username}")
    private String auth_token_username;

    @Value("${pcmt_auth_token_password}")
    private String auth_token_password;

    @Autowired
    private InterfaceMapper repository;

    @Autowired
    private AsyncComponentService asyncComponentService;

    public User getAll() {
        User u = repository.getUsers();
        return repository.getUsers();
    }

   //@Scheduled(cron = "${batch.job.send.dhis.data}")
    public void postDHIS2DATA() {
      //  asyncComponentService.asyncDhis2Expected();
    }



   // @Scheduled(cron = "${batch.job.send.thscp.data}")

   // @Scheduled(fixedRate=60*60*1000, initialDelay=5000)
    public void postTHSCPData() throws EncoderException, IOException {
      //  asyncComponentService.pullFacilities();
      // asyncComponentService.testPagination();
      //  getPCMTProductByCode();


       // asyncComponentService.pullFacilities();

      //  asyncComponentService.asyncProcessStockAvailability();
       // asyncComponentService.asyncProcessTurnAroundTime();

      //  asyncComponentService.asyncProcessPercentageWastage();

        //Worked
       // asyncComponentService.asyncProcessTimelinessReporting();
      //  asyncComponentService.asyncPercentageRequisitionData();


       //asyncComponentService.asyncProcessProductList();
        //This worked

       // asyncComponentService.asyncProcessPrograms();
        //tHIS WORKED

        // asyncComponentService.testPagination();
        //asyncComponentService.asyncProcessPrograms();

        //Commented, will be allowed after testing is done
      //  asyncComponentService.asyncProcessStockOnHand();
       // asyncComponentService.asyncEmergencyCommodities();
      //  asyncComponentService.asyncForecastingData();
    }

    @Scheduled(fixedRate=60*60*1000, initialDelay=5000)
    public void ScheduledMethod() throws IOException {
      //  asyncComponentService.methodOne();
        //TESTED
     //  asyncComponentService.asyncDhis2Expected();
     //  asyncComponentService.methodTwo();
     //  asyncComponentService.methodThree();
      // asyncComponentService.methodFour();
       // asyncComponentService.methodFive();

        //To work on the data
        asyncComponentService.sendLLINData();
    }

  // @Scheduled(fixedRate=10000, initialDelay=500)
    public void sendDataFromDB() throws IOException {
      //  asyncComponentService.sendDataFromDB();
    }

    private PMCTAccessToken getPCMTAuthenticationToken() {

        PMCTAccessToken pmctAccessToken = null;

        String url = PMCT_BASE_URL+token_api;

        String plainCreds = auth_token_username+":"+auth_token_password;
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Creds);

        headers.setContentType(MediaType.APPLICATION_JSON);

        AuthParameterDto params = new AuthParameterDto();

        HttpEntity<String> request = new HttpEntity<>(
            params.toString(), headers);

        ignoreCertificates();
        ResponseEntity<String> response = restTemplate
            .exchange(url, HttpMethod.POST, request , String.class );

        ObjectMapper objectMapper = new ObjectMapper();

        try {

             pmctAccessToken = objectMapper.readValue(response.getBody(), PMCTAccessToken.class);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return pmctAccessToken;
    }

    public void getPCMTProductByCode() throws EncoderException {

        List <org.openlmis.integration.dto.Product> prod = repository.getProductList();

        PMCTAccessToken token = getPCMTAuthenticationToken();
        System.out.println(token);

        System.out.println("Get PCMT token first");

        if (token != null) {

            String url = PMCT_BASE_URL + product_api;

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token.getAccess_token());

            HttpEntity<String> entity = new HttpEntity<String>(headers);

            System.out.println("Ignore Check for SSL certificate to get PCMT connection");

            ignoreCertificates();
            ResponseEntity<String> responseEntity = restTemplate
                    .exchange(url, HttpMethod.GET, entity, String.class);

            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            try {

               // System.out.println(responseEntity);
                System.out.println("Map PCMT Response Body to get the list of products");
                PCMTResponseBody responseBody = mapper.readValue(responseEntity.getBody(), PCMTResponseBody.class);

                String dataV = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseEntity.getBody());

                List<Item> items;

                items = responseBody.getEmbedded().getItems();
                OrderableDto orderableDto = null;

                List<OrderableDto> orderableDtoList = new ArrayList<>();

                if(!items.isEmpty()) {
                    for (Item item : items) {

                        orderableDto = OrderableBuilder.build(item);
                        orderableDtoList.add(orderableDto);

                    }
                }

                List<org.openlmis.integration.dto.Product> intersection = prod.stream()
                    .filter(p1 -> orderableDtoList.stream().anyMatch(p2 -> p2.getProductCode().equals(p1.getProductCode())))
                    .collect(Collectors.toList());

                String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(items);

                List<ProductTest> productTestList = repository.getAllP();

                for (ProductTest productTest: productTestList) {

                    for (OrderableDto dto: orderableDtoList) {

                        if(dto.getProductCode().equals(productTest.getCode())){
                            System.out.println(dto.getProductCode());
                            System.out.println("Found Match");
                        }
                    }
                }


             //   System.out.println(indented);

            } catch (JsonProcessingException e) {

                System.out.println("Error in processing the JSON");
            }

           // System.out.println(responseEntity.getBody());

           // System.out.println(headers);
        }
    }


/*protected RestTemplate configureSSLCertification()
    throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

    TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

    SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
        .loadTrustMaterial(null, acceptingTrustStrategy)
        .build();

    SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

    CloseableHttpClient httpClient = HttpClients.custom()
        .setSSLSocketFactory(csf)
        .build();

    HttpComponentsClientHttpRequestFactory requestFactory =
        new HttpComponentsClientHttpRequestFactory();

    requestFactory.setHttpClient(httpClient);

    return new RestTemplate(requestFactory);
}*/


private void ignoreCertificates() {
    TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
        @Override
        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
    }};

    try {
        SSLContext sc = SSLContext.getInstance("TLS");
        sc.init(null, trustAllCerts, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    } catch (Exception e) {

    }

}


}

package org.openlmis.integration.controller;

import org.openlmis.integration.dto.User;
import org.openlmis.integration.service.AsyncComponentService;
import org.openlmis.integration.service.InterfaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class InterfaceController {

    @Autowired
    private InterfaceService service;

    @Autowired
    private AsyncComponentService componentService;

    @RequestMapping(value="/triggerManually", method= RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Object> getAll() {
       User entityList = service.getAll();

        return new ResponseEntity<Object>(entityList, HttpStatus.OK);
    }
}

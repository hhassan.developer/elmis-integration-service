package org.openlmis.integration.dto;

import lombok.Data;

@Data
public class ProductTest {
  private String primaryName;

  private String code;

  private Long id;

}

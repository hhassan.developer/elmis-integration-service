package org.openlmis.integration.dto;

import lombok.Data;

@Data
public class Product {

    private String uuid;

    private String name;

    private String category;

    private String description;

    private String productCode;

    private String productUnit;

}

package org.openlmis.integration.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "paging",
        "Count",
        "FacilityData",
})
@Data
public class HealthFacility {

    @JsonProperty("paging")
    private Paging page;

    @JsonProperty("Count")
    private Integer count;

    @JsonProperty("FacilityData")
    private List<HealthFacilityDTO> facilityData;

}

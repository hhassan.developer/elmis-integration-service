package org.openlmis.integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonPropertyOrder({
        "page",
        "pageSize",
        "Total",
})
@Data
public class Paging {

  Integer page;
  Integer pageSize;
  @JsonProperty("Total")
  Integer total;
}

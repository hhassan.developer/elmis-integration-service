package org.openlmis.integration.dto.apidto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;import org.openlmis.integration.dto.apidto.Price;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class PriceReference {

  @JsonProperty("locale")
  public String locale;
  @JsonProperty("scope")
  public String scope;
  @JsonProperty("data")
  public List<Price> data;

}
package org.openlmis.integration.dto.apidto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class PCMTProduct {

    @JsonProperty("identifier")
    private String identifier;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("family")
    private String family;

}

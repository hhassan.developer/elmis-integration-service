package org.openlmis.integration.dto.apidto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthParameterDto {

  private String grantType="password";
  private String username="admin";
  private String password="Admin123";

  @Override
  public String toString() {

    return "{\"grant_type\":\"" +this.grantType+"\""+
        ",\"username\":\"" +this.username+"\""+
        ",\"password\":\"" +this.password+"\""+
        "}";
  }
}

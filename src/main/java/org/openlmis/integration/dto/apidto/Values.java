package org.openlmis.integration.dto.apidto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;import org.openlmis.integration.dto.apidto.BaseValue;
import org.openlmis.integration.dto.apidto.PriceReference;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Values {

  @JsonProperty("PACK_SIZE")
  public List<BaseValue> packSize;
  @JsonProperty("price_reference")
  public List<PriceReference> priceReferences;
  @JsonProperty("BASE_UOM")
  public List<BaseValue> baseUom;
  @JsonProperty("LMIS_CODE")
  public List<BaseValue> lmisCode;
  @JsonProperty("LMIS_UUID")
  public List<BaseValue> lmisUuid;
  @JsonProperty("PRODUCT_DESCRIPTION")
  public List<BaseValue> productDescription;
  @JsonProperty("UOM_QTY_FACTOR")
  public List<BaseValue> uomQtyFactor;
  @JsonProperty("LMIS_PACK_ROUNDING_THRESHOLD")
  public List<BaseValue> lmisPackRoundingThreshold;
  @JsonProperty("LMIS_ROUND_TO_ZERO")
  public List<BaseValue> lmisRoundToZero;

}
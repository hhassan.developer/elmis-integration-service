package org.openlmis.integration.dto.apidto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Item {

  @JsonProperty("identifier")
  public String identifier;
  @JsonProperty("family")
  public String family;
  @JsonProperty("parent")
  public Object parent;
  @JsonProperty("categories")
  public List<String> categories;
  @JsonProperty("values")
  public Values values;
  @JsonProperty("created")
  public String created;
  @JsonProperty("updated")
  public String updated;

}
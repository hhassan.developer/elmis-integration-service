package org.openlmis.integration.dto;

import lombok.Data;

@Data
public class PercentageRejectedRequisition {

    private String program;

    private String facilityId;

    private String period;

    private Integer rejectedForms;

    private String submittedAt;

    private Integer submittedForms;

    private String uuid;
}

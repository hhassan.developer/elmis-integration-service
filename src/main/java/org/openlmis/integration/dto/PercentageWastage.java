package org.openlmis.integration.dto;

import lombok.Data;

@Data
public class PercentageWastage {

    private String uuid;
    private String facilityId;
    private Integer facilityLevel;
    private String period;
    private String productCode;
    private String programCode;
    private Integer quantity;
    private Float damagedPercentage;
    private Float expiredPercentage;
    private Float lostPercentage;
}

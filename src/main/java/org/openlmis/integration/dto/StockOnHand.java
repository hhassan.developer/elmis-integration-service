package org.openlmis.integration.dto;

import lombok.Data;

@Data
public class StockOnHand {

    private String uuid;
    private Integer consumedQuantity;
    private String facilityId;
    private Integer facilityLevel;
    private Integer monthsOfStock;
    private String period;
    private String productCode;
    private String programCode;
    private Integer quantity;
    private Integer stockId;
    private Float damagedPercentage;
    private Float expiredPercentage;
    private Float lostPercentage;

}

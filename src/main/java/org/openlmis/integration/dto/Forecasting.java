package org.openlmis.integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Forecasting {

    private String productCode;

    private String programCode;

    private String facilityId;

    private String period;

    private Integer consumedQuantity;

    private Integer forecastQuantity;

    private String uuid;

}

package org.openlmis.integration.dto;

import lombok.Data;

@Data
public class TurnAroundTime {

    private String uuid;
    private Integer deliveredQuantity;
    private String deliveryDate;
    private String deliveryFromFacilityId;
    private String deliveryPromiseDate;
    private String orderDate;
    private String orderFromFacilityId;
    private String orderId;
    private String orderStatus;
    private String orderType;
    private Integer orderedQuantity;
    private String productCode;
    private String programCode;
    private Integer targetDays;

}

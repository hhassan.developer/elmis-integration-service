package org.openlmis.integration.dto;

import lombok.Data;

@Data
public class LogResponse {
  private Log id;
  private String jsonString;
  private String errorString;
}

package org.openlmis.integration.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class PMCTAccessToken {

    private String access_token;

    private Integer expires_in;

    private String token_type;

    private String refresh_token;

    private String scope;
}

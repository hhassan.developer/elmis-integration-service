package org.openlmis.integration.dto;

import lombok.Data;

@Data
public class Program {

    private String uuid;
    private String name;
    private String description;
    private String programCode;

}

package org.openlmis.integration.dto;

import lombok.Data;

@Data
public class TimelinessReporting {

    private String uuid;

    private Integer expected;

    private String period;

    private String program;

    private Integer nonReported;

    private Integer unscheduled;

    private String districtCode;

    private Integer reportedLate;

    private Integer reported;
}

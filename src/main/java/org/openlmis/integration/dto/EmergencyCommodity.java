package org.openlmis.integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class EmergencyCommodity {

    private String productCode;
    private String programCode;
    @JsonProperty("facility_id")
    private String facilityId;
    private String period;
    private Integer availableQuantity;
    private Integer stockQuantity;
    private Integer stockOfMonth;
    private String uuid;

}

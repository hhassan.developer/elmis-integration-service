package org.openlmis.integration.dto;

import org.openlmis.integration.dto.apidto.Item;

public final class OrderableBuilder {

/**
 * Maps Item to OrderableDto.
 *
 * @return
 */
public static OrderableDto build(Item item) {

  OrderableDto orderableDto = new OrderableDto();
  orderableDto.setUom(item.getValues().getBaseUom().get(0).getData());
  orderableDto.setRevision(item.getValues().getProductDescription().get(0).getData());
  if(item.getValues().getLmisCode() == null){
    orderableDto.setProductCode(item.getIdentifier());
  } else
  orderableDto.setProductCode(item.getValues().getLmisCode().get(0).getData());

  return orderableDto;
}

private OrderableBuilder() {
}

}
package org.openlmis.integration.dto;

import lombok.Data;

@Data
public class ZoneDto {
    private Long zoneId;
}

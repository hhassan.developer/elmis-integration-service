package org.openlmis.integration.dto.dhis;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ElmisInterfaceDto {

  private List<ElmisInterfaceDataSetDto> dataValues;

}
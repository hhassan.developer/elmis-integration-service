package org.openlmis.integration.dto.dhis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ElmisInterfaceDataSetDto {
  private String dataElement;
  private Long period;
  private String orgUnit;
  private Long value;
}
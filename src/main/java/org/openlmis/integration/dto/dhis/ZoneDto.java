package org.openlmis.integration.dto.dhis;


import lombok.Data;

@Data
public class ZoneDto {
  private Long zoneId;

  private Long facilityId;

  private String hfrCode;

}


package org.openlmis.integration.dto.dhis;

import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class DataSetDto {

  private String completeDate;
  private Long period;
  private String orgUnit;
  private String dataSet;
  private List<DataSetElement> dataValues;

}

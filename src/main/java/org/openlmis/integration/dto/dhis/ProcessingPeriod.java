package org.openlmis.integration.dto.dhis;

import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

/**
 * ProcessingPeriod represents the time period belonging to a particular schedule according to which requisition life cycle
 * will be followed.
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonSerialize(include = NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessingPeriod {
  private Long id;
  private Long scheduleId;
  private String name;
  private String description;
  private Integer numberOfMonths;

  @JsonDeserialize(using = DateDeserializers.DateDeserializer.class)
  private Date startDate;

  @JsonDeserialize(using = DateDeserializers.DateDeserializer.class)
  private Date endDate;

  private Boolean enableOrder;

  private Boolean isReporting;

  public ProcessingPeriod(Long id) {
    this.id = id;
  }

  public ProcessingPeriod(Long id, Date startDate, Date endDate, Integer numberOfMonths, String name) {
    this.id = id;
    this.startDate = startDate;
    this.endDate = endDate;
    this.numberOfMonths = numberOfMonths;
    this.name = name;
  }

  public void includeEntireDuration() throws ParseException {
    SimpleDateFormat dateFormatWithoutTime = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat dateFormatWithTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    startDate = dateFormatWithoutTime.parse(dateFormatWithoutTime.format(startDate) + " 00:00:00");
    endDate = dateFormatWithTime.parse(dateFormatWithoutTime.format(endDate) + " 23:59:59");
  }

  @SuppressWarnings("unused")
  public String getStringStartDate() throws ParseException {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    return simpleDateFormat.format(this.startDate);
  }

  @SuppressWarnings("unused")
  public String getStringEndDate() throws ParseException {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    return simpleDateFormat.format(this.endDate);
  }

  public String getStringYear(){
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
    return simpleDateFormat.format(this.startDate);
  }

}
package org.openlmis.integration.dto.dhis;

import lombok.Data;

@Data
public class ProgramDto {

  private String code;

  private Long id;

  private String name;

}

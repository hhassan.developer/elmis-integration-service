package org.openlmis.integration.dto;

import lombok.Data;

@Data
public class StockAvailability {
    private String uuid;
    private String facilityId;
    private String incident;
    private String period;
    private String productCode;
    private String programCode;
}

package org.openlmis.integration.dto;

import lombok.Data;

@Data
public class Log {

  private Long id;
  private String jsonString;
  Boolean sent;

}

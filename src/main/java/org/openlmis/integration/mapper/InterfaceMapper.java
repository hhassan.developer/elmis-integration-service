package org.openlmis.integration.mapper;

import org.apache.ibatis.annotations.*;
import org.openlmis.integration.dto.*;
import java.util.List;
import org.openlmis.integration.dto.dhis.DataSetElement;
import org.openlmis.integration.dto.dhis.ElmisInterfaceDataSetDto;
import org.openlmis.integration.dto.dhis.ProcessingPeriod;
import org.openlmis.integration.dto.dhis.ProgramDto;
import org.openlmis.integration.dto.dhis.ZoneDto;

@Mapper
public interface InterfaceMapper {

    @Select("SELECT * FROM USERS limit 1")
    public User getUsers();

    @Select(" select uuid_generate_v4() uuid, pr.code as \"programCode\",  p.alternateitemcode as \"productCode\", f.hfrcode facilityId,  to_char(r.modifieddate, 'yyyy-MM-dd') as \"period\",\n" +
            "stockinhand as \"availableQuantity\", quantityReceived as \"stockQuantity\", case when amc > 0 then stockinhand/ amc else 0 end as \"stockOfMonth\"\n" +
            "from requisitions r\n" +
            "JOIN requisition_line_items i On r.id = i.rnrid\n" +
            "JOIN orders o ON r.id = o.id\n" +
            "JOIN products p ON i.productcode = p.code\n" +
            "JOIN program_products pp On pp.productid = p.id" +
            " JOIN programs pr ON pp.programId = pr.id" +
            " JOIN facilities f ON r.facilityId = F.ID" +
            "\t\t\t\t\t\t   join hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n" +
            "                           where  r.programid in(1,8) and r.periodid=#{periodId}\n" +

            " and hrf.operatingstatus = 'Operating' and " +
            " f.hfrcode is not null and p.alternateitemcode is not null and stockinhand > 0 " +
        " and p.alternateitemcode not in('40030065','20130010MD') \n"
    )
    List<EmergencyCommodity> getThScpEmergencyCommodities(@Param("periodId") Long periodId);

    @Select(" SELECT hrf.facidnumber as facilityId, alternateitemcode productcode,\n" +
            "            uuid_generate_v4() uuid,\n" +
            "            coalesce(quantityDispensed, 0) consumedQuantity,\n" +
            "            to_char(pp.enddate,'YYYY-MM-dd') as period,\n" +
            "                    coalesce(normalizedconsumption,0) forecastQuantity,\n" +
            "              p.code as programCode\n" +
            "              \n" +
            "              from requisitions r\n" +
            "              JOIN requisition_line_items i on r.id = i.rnrid\n" +
            "              JOIN facilities F on r.facilityId = F.ID\n" +
            "              LEFT OUTER JOIN requisition_line_item_losses_adjustments L on l.requisitionlineitemid = i.id\n" +
            "              JOIN processing_periods pp on r.periodId = pp.id\n" +
            "                    left  JOIN losses_adjustments_types ft ON l.type = ft.name\n" +
            "\t\t\t\t\tJOIN prODUCTS pD ON I.PRODUCTCODE = PD.CODE\n" +
            "            JOIN programs p on r.programId = P.ID\n" +
            "               join hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n" +
            "                                     where  r.programid in(1,8) and r.periodid =#{periodId}\n" +
            "           and hrf.operatingstatus = 'Operating' \n" +
            "\t\t   AND pD.alternateitemcode is not null\n" +
            "            and quantityDispensed > 0 \n" +
            "\t\t\tand pD.alternateitemcode not in('40030065')\n" +
            "            ORDER BY R.ID DESC ")
    public List<Forecasting> getForeCastedData(@Param("periodId") Long periodId);

    @Select(" \n" +
            "SELECT uuid_generate_v4() uuid, \n" +
            "    X.periodID," +
            "   to_char(x.period, 'yyyy-MM-dd') period, \n" +
            "\tX.program,\n" +
            "\tcount(*) rejectedForms, \n" +
            "\tto_char(x.createdDate, 'yyyy-MM-dd') submittedAt, \n" +
            "    facilityCode as facilityId,\n" +
            "\n" +
            "\n" +
            "\t\t (select count(*) from facilities \n" +
            "\t\tjoin programs_supported ps on ps.facilityId = facilities.id \n" +
            "\t\t join geographic_zones gz on gz.id = facilities.geographicZoneId \n" +
            "\t\t where  ps.programId = programId and facilities.id in \n" +
            "\t\t (select facilityId from requisitions JOIN FACILITIES fAC ON REQUISITIONS.FACILITYID = FAC.ID " +
            " where periodId =x.periodid and programId = programid and status not in ('INITIATED', 'SUBMITTED', 'SKIPPED') \n" +
            "\t\t  and emergency = false  and FAC.code = x.facilitycode )  AND geographicZoneId = X.DISTRICT_ID AND facilities.code = x.facilitycode \n" +
            "\t\t ) AS submittedForms\n" +
            "    \n" +
            "FROM (\n" +
            "   select pp.startDate period, pp.name periodname, periodID, f.hfrcode facilityCode ,programs.code as program, count(*), max(c.modifieddate) as createdDate, rnrid,\n" +
            " DISTRICT_ID, DISTRICT_NAME, programId from requisition_status_changes c \n" +
            "   join requisitions r on r.id = c.rnrid \n" +
            "    join programs on programs.id = r.programid\n" +
            "    join facilities f on f.id = r.facilityid \n" +
            "    join vw_districts d on d.district_id = f.geographiczoneid \n" +
            "\tJOIN processing_periods pp ON r.periodid = pp.id\n" +
            "\t\t\t\t\t\t   join hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n" +
            "                           where  r.programid in(1,8) and r.periodid =#{periodId}\n" +

            " and hrf.operatingstatus = 'Operating' and " +
            "     C.status in('INITIATED')  and r.emergency = false\n" +
            "    \n" +
        "\t\t\t\t\t\t\tAND f.hfrcode is not null "+
            "   group by pp.startDate, rnrid,programs.code,f.hfrcode,pp.name, DISTRICT_ID, DISTRICT_NAME,r.periodid,PROGRAMID having count(*) > 1\n" +
            "\t\n" +
            "\t\n" +
            "\t)X " +
          //  " where x.facilitycode = '103854-6' " +
            "GROUP BY x.periodid,x.period,  program,createdDate,rnrid, DISTRICT_NAME, x.district_id,facilityCode \n" +

            "\t ")
    public List<PercentageRejectedRequisition> getPercentageRejected(@Param("periodId") Long periodId);

/*
    @Select(" select uuid_generate_v4() uuid, district_name district, \n" +
            "p.code as program,\n" +
            "to_char(pp.startdate, 'yyyy-MM-dd') period\n" +
            ", count(*) expected from facilities  \n" +
            "    join programs_supported ps on ps.facilityId = facilities.id \n" +
            "     join geographic_zones gz on gz.id = facilities.geographicZoneId \n" +
            "     join requisition_group_members rgm on rgm.facilityId = facilities.id \n" +
            "     join requisition_group_program_schedules rgps on rgps.requisitionGroupId = rgm.requisitionGroupId and rgps.programId = ps.programId  \n" +
            "     join processing_periods pp on pp.scheduleId = rgps.scheduleId \n" +
            "     JOIN programs p ON ps.programId = p.id\n" +
            "     JOIN vw_districts d on gz.id = d.district_id\n" +
            "     where gz.levelId = (select max(id) from geographic_levels) and ps.programId in(8,1)\n" +
            "     and ps.startdate <= (SELECT startDate from processing_periods where id =24)\n" +
            "     \n" +
            "     group by  district_name , p.code,pp.startdate LIMIT 10000")
*/


    @Select("               SELECT  districtcode,\n" +
        "               (SELECT startDate from processing_periods where id =  #{periodId})::date period,\n" +
        "\t\t\t   (SELECT code from programs where id = 8) as program, \n" +
        "                uuid_generate_v4() uuid,\n" +
        "                COALESCE(expected.count,0) expected,COALESCE(reported.total,0) reported,\n" +
        "\t\t\t\tCOALESCE(lateReported.total,0) reportedLate,COALESCE(unscheduled.total,0) unscheduled,  \n" +
        "\t\t\t\tCOALESCE(expected.count,0) - (COALESCE(reported.total,0) + COALESCE(lateReported.total,0) + COALESCE(unscheduled.total,0) ) \n" +
        "\t\t\t\tnonReported\n" +
        "\n" +
        "                 FROM    \n" +
        "                 geographic_zoneS gzz \n" +
        "\t\t\t\t \n" +
        "\t\t\tLEFT JOIN vw_districts on gzz.id= vw_districts.district_id  \n" +
        "            JOIN facilities f ON f.geographiczoneid = gzz.id\n" +
        "            join hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n" +
        "                 LEFT JOIN  \n" +
        "                (\n" +
        "\t\t\t\tSELECT count(*) total,geographiczoneId \n" +
        "                from vw_timeliness_report \n" +
        "                JOIN vw_districts on geographiczoneId= vw_districts.district_id   \n" +
        "\n" +
        "                WHERE  reportingStatus='R'  \n" +
        "\t\t\t\t\t\tand programId = 8 AND PERIODiD = #{periodId}\n" +
        "                GROUP BY geographiczoneId,vw_timeliness_report.reportingstatus \n" +
        "\t\t\t\t \n" +
        "                )reported ON GZZ.ID= reported.geographiczoneId \n" +
        "                LEFT JOIN \n" +
        "                ( \n" +
        "                SELECT count(*) total,geographiczoneId \n" +
        "                from vw_timeliness_report \n" +
        "                JOIN vw_districts on geographiczoneId= vw_districts.district_id    \n" +
        "                WHERE  reportingStatus='U' and programId = 8 AND PERIODiD =  #{periodId} \n" +
        "                GROUP BY geographicZoneId,vw_timeliness_report.reportingstatus \n" +
        "                )unscheduled ON GZZ.ID = unscheduled.geographicZoneId \n" +
        "                LEFT JOIN \n" +
        "                ( \n" +
        "                SELECT count(*) total,geographicZoneId  \n" +
        "\n" +
        "                from vw_timeliness_report \n" +
        "                JOIN vw_districts on geographicZoneId= vw_districts.district_id   \n" +
        "                where reportingStatus='L'   \n" +
        "\t\t\t\t\tand programId = 8 AND PERIODiD =  #{periodId} \n" +
        "                GROUP BY geographicZoneId,vw_timeliness_report.reportingstatus  \n" +
        "\t\t\t\t\t\n" +
        "                )lateReported ON GZZ.ID = lateReported.geographicZoneId  \n" +
        "               \n" +
        "\t\t\t   -- clean\n" +
        "\t\t\t   LEFT JOIN  \n" +
        "                      (\n" +
        "\t\t\t\t\t\t  select geographicZoneId, count(*) from VW_EXPECTED_FACILITIES   \n" +
        "                     JOIN vw_districts on VW_EXPECTED_FACILITIES.geographiczoneId = vw_districts.district_id  \n" +
        "                      where  programId = 8 AND PERIODiD =  #{periodId} \n" +
        "                      group by geographicZoneId  \n" +
        "\t\t\t\t\t\t  \n" +
        "\t\t\t\t\t   \n" +
        "                 ) expected on gzz.id = expected.geographiczoneId  \n" +
        "\t\t\t\t \n" +
        "               WHERE reported.total> 0 OR unscheduled.total>0 OR lateReported.total >0 and expected.count > 0  \n" +
        "\t\t\t   and vw_districts.parent is not null    \n" +
        "            and hrf.districtcode is not null and hrf.districtcode <> ''\n" +
        "                group by  GZZ.ID,reported.total,unscheduled.total,lateReported.total, GZZ.NAME,expected.count,\n" +
        "                vw_districts.region_name,vw_districts.zone_name ,hrf.districtcode \n" +
        "                order by vw_districts.zone_name\n"
    )

    List<TimelinessReporting> getTimelinessReporting(@Param("periodId") Long periodId);

    @Select("    select uuid_generate_v4() uuid, o.orderNumber as orderId, p.alternateitemcode as productCode, f.hfrCode as orderFromFacilityId,\n" +
            "            case when (r.emergency = false) then 'Emergency' else 'Regular' end as orderType ,\n" +
            "             CASE WHEN QuantityApproved IS NULL THEN 0 ELSE QuantityApproved END as orderedQuantity,\n" +
            "             CASE WHEN quantityReceived IS NULL THEN 0 ELSE quantityReceived END as deliveredQuantity, to_char(r.createdDate, 'yyyy-MM-dd') as orderDate,\n" +
            "            \n" +
            "            to_char(r.modifieddate, 'yyyy-MM-dd') as deliveryPromiseDate,\n" +
            "            to_char(r.modifieddate, 'yyyy-MM-dd') as deliveryDate,\n" +
            "            LEFT(f.code, 2) as deliveryFromFacilityId,\n" +
            "            r.status as orderStatus,\n" +
            "            30 as targetDays, pr.code as programCode\n" +
            "            from requisitions r\n" +
            "            JOIN requisition_line_items i On r.id = i.rnrid\n" +
            "            JOIN orders o ON r.id = o.id\n" +
            "            JOIN products p ON i.productcode = p.code\n" +
            "            JOIN program_products pp On pp.productid = p.id " +
            "            JOIN programs pr ON pp.programId = pr.id   \n" +
            "             JOIN facilities f ON r.facilityId = F.ID \n" +
            "             JOIN processing_periods per ON r.periodId = per.id \n" +
            "\t\t\t\t\t\t join hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n" +
            "             where  r.programid in(1,8) and r.periodid = #{periodId}\n" +

            " and hrf.operatingstatus = 'Operating' and " +
            "              p.alternateitemcode is not null and " +
        " quantityApproved > 0 and pr.active = true " +
        "\t\t\t\t\t\t\tAND f.hfrcode is not null and p.alternateitemcode is not null" +
        " and i.productcode is not null \n" +
            "    and p.description  is not null and p.description != ''\n" +
            "             ORDER BY R.ID DESC ")
    List<TurnAroundTime> getTurnAroundTime(@Param("periodId") Long periodId);

    @Select("  select  uuid_generate_v4() uuid, " +
            "  hfrcode facilityId, status as incident,\n" +
            "  \n" +
            "  to_char(PP.STARTDATE, 'yyyy-MM-dd') as period, " +
            "  p.alternateitemcode productCode, program programCode\n" +
            "  from mv_stock_imbalance_by_facility_report R" +
        "   JOIN facilities F on facilitycode = f.code" +
        "  JOIN products p on r.productcode = p.code " +
            "\t\t\t\t\t\t   join hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n"+
            " JOIN processing_periods pp ON R.PERIODID = PP.ID  where " +
            " \t\t\t\t\t\t\t   hrf.operatingstatus = 'Operating' and\n r.programid in(1,8) and pp.id = #{periodId} and " +
        "  hfrcode is not null and p.alternateitemcode is not null order by PP.STARTDATE DESC " +
            " ")
    List<StockAvailability> getStockAvailability(@Param("periodId") Long periodId);

    @Select(" SELECT   uuid_generate_v4() uuid, "+
            "           p.alternateitemcode as productCode, " +
            "            p.primaryName as name, "+
            "            p.description,  p.dispensingUnit as productUnit,  " +
            "          case when pc.code = 'msus' then 'medical supplies' else 'pharmaceuticals' end as category "+
            "          FROM program_products pP "+
            "          JOIN products p ON p.id = pp.productId "+
            "          join programs pr ON pr.id = pp.programId "+
            "          join product_categories pc ON pp.productcategoryid = pc.id" +
            " where p.description  is not null and p.description != '' and p.alternateitemcode is not null ")
    List<Product> getProductList();

    @Select(" SELECT uuid_generate_v4() uuid,\n" +
            "\t\t\t\t\t\t f.hfrcode as facilityId,\n" +
            "\t\t\t\t\t\t quantitydispensed consumedQuantity,\n" +
            "\t\t\t\t\t\t 2 facilityLevel,\n" +
            "\t\t\t\t\t\t Case when amc > 0 THEN stockInHand / amc else 0 end as monthsOfStock,\n" +
            "\t\t\t\t\t\tto_char(r.modifieddate, 'yyyy-MM-dd') period,\n" +
            "\t\t\t\t\t\tp.alternateitemcode as productCode,\n" +
            "\t\t\t\t\t\tpr.code programCode,\n" +
            "\t\t\t\t\t\n" +
            "\t\t\t\t\t\t CASE WHEN totallossesandadjustments IS NULL THEN 0 ELSE totallossesandadjustments END as quantity,\n" +

            "\t\t\t\t\t\tCOALESCE((\n" +
            "\t\t\t\t\t    SELECT\n" +
            "\t\t\t\t\t\t CASE WHEN (beginningBalance + quantityReceived)> 0 THEN\n" +
            "\t\t\t\t\t\tquantity / (beginningBalance + quantityReceived) ELSE 0 END AS  lostPercentage\n" +
            "\t\t\t\t\t\t \n" +
            "\t\t\t\t\t\tfrom requisition_line_item_losses_adjustments item where item.type in('STOLEN')\n" +
            "\t\t\t\t\t\t  AND  ITEM.requisitionlineitemid = i.ID\n" +
            "\t\t\t\t\t\t),0) lostPercentage,\n" +
            "\t\t\t\t\t\t\n" +
            "\t\t\t\t\t\tCOALESCE((\n" +
            "\t\t\t\t\t    SELECT\n" +
            "\t\t\t\t\t\t CASE WHEN (beginningBalance + quantityReceived)> 0 THEN\n" +
            "\t\t\t\t\t\tquantity / (beginningBalance + quantityReceived) ELSE 0 END AS  damagedPercentage\n" +
            "\t\t\t\t\t\t \n" +
            "\t\t\t\t\t\tfrom requisition_line_item_losses_adjustments item where item.type in('DAMAGED')\n" +
            "\t\t\t\t\t\t  AND  ITEM.requisitionlineitemid = i.ID\n" +
            "\t\t\t\t\t\t),0) damagedPercentage,\n" +
            "\t\t\t\t\t\t\n" +
            "\t\t\t\t\t\t\tCOALESCE((\n" +
            "\t\t\t\t\t    SELECT\n" +
            "\t\t\t\t\t\t CASE WHEN (beginningBalance + quantityReceived)> 0 THEN\n" +
            "\t\t\t\t\t\tquantity / (beginningBalance + quantityReceived) ELSE 0 END AS  expiredPercentage\n" +
            "\t\t\t\t\t\t \n" +
            "\t\t\t\t\t\t from requisition_line_item_losses_adjustments item where item.type in('EXPIRED')\n" +
            "\t\t\t\t\t\t  AND  ITEM.requisitionlineitemid = i.ID\n" +
            "\t\t\t\t\t\t),0) expiredPercentage\n" +
            "\t\t\t\t\n" +
            "                        from requisitions r\n" +
            "                        JOIN requisition_line_items i On r.id = i.rnrid\n" +
            "                        JOIN orders o ON r.id = o.id\n" +
            "                        JOIN products p ON i.productcode = p.code\n" +
            "                        JOIN program_products pp On pp.productid = p.id  \n" +
            "                        JOIN programs pr ON pp.programId = pr.id  \n" +
            "                        JOIN facilities f ON r.facilityId = F.ID\n" +
            "\t\t\t\t\t\t   join hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n"+

            "                        JOIN processing_periods per ON r.periodId = per.id\n" +
            "                        WHERE  quantityApproved > 0  AND pr.active = true\n" +
            " \t\t\t\t\t\t\t AND  hrf.operatingstatus = 'Operating' \n" +
            "\t\t\t\t\t\t\t    and  pr.id in(1,8) and r.periodid=#{periodId}\n" +

            "\t\t\t\t\t\t\tAND f.hfrcode is not null and p.alternateitemcode is not null" +
             " and totallossesandadjustments > 0  and p.alternateitemcode  not in('40030169')"+
            "                         ORDER BY R.ID DESC \n")
    List<PercentageWastage> getPercentageWastage(@Param("periodId") Long periodId);

    @Select(" SELECT uuid_generate_v4() uuid,\n" +
            "\t\t\t\t\t\t f.hfrcode as facilityId,\n" +
            "\t\t\t\t\t\t quantitydispensed consumedQuantity,\n" +
            "\t\t\t\t\t\t 2 facilityLevel,\n" +
            "\t\t\t\t\t\tto_char(r.modifieddate, 'yyyy-MM-dd') period,\n" +
            "\t\t\t\t\t\tp.alternateitemcode as productCode,\n" +
            "\t\t\t\t\t\tpr.code programCode,\n" +
            "\t\t\t\t\t\t CASE WHEN stockInHand IS NULL THEN 0 ELSE stockInHand END as quantity,\n" +
            "\t\t\t\t\t\t r.id stockId\n" +
            "                        from requisitions r\n" +
            "                        JOIN requisition_line_items i On r.id = i.rnrid\n" +
            "                        JOIN orders o ON r.id = o.id\n" +
            "                        JOIN products p ON i.productcode = p.code\n" +
            "                        JOIN program_products pp On pp.productid = p.id  \n" +
            "                        JOIN programs pr ON pp.programId = pr.id  \n" +
            "                        JOIN facilities f ON r.facilityId = F.ID\n" +
            "\t\t\t\t\t\t   join hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n"+

            "                        JOIN processing_periods per ON r.periodId = per.id\n" +
            "                        WHERE  quantityApproved > 0  AND pr.active = true\n" +
            " \t\t\t\t\t\t\t AND  hrf.operatingstatus = 'Operating' \n" +
            "\t\t\t\t\t\t\t    and  pr.id in(1,8) and r.periodid = #{periodId} \n" +

            "\t\t\t\t\t\t\tAND f.hfrcode is not null and p.alternateitemcode is not null" +
            " and p.alternateitemcode NOT in('40030065')" +
        " " +

            "                         ORDER BY R.ID DESC \n")
    List<StockOnHand> getStockOnHand(@Param("periodId") Long periodId);

    @Select("Select code as programCode, name, description, uuid_generate_v4() uuid from programs ")
    List<Program> getAllPrograms();

/*    @Select(" select * from processing_periods where   \n" +
       "             startdate::date >= '2021-09-01'::date and startdate <= '2022-01-31'::date\n" +
        "        order by id desc")*/

 /*       @Select("select * from processing_periods where   \n" +
            "startdate::date >= '2018-03-01'::date and endDate < NOW()::date\n" +
            "and scheduleid <> 47\n" +
            "and numberofmonths > 0 and id = 189\n" +
            "order by id desc")*/
     @Select("select * from processing_periods where   \n" +
         "startdate::date >= '2020-01-01'::date and endDate < NOW()::date\n" +
         "and scheduleid <> 47\n" +
         "and numberofmonths > 0 \n" +
         "order by id desc")
    //@Select("\tselect * from processing_periods where startdate::date >= '2021-01-01'::date and startdate <= now()::date\n \n")
    List<ProcessingPeriod> getByStartDate();

    @Select("\tselect distinct zone_id zoneId, district_id, f.id facilityId, hfrcode\n" +
        "              from vw_districts  d\n" +
        "                       JOIN facilities F on d.district_id = f.geographiczoneid \n" +
        "                     LEFT join hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n" +
        "\t\t\t\t\t JOIN facility_types ft on f.typeid = ft.id\n" +
        "                         where zone_id not in(437) AND\n" +
        "                        f.active = true and f.enabled = true \n" +
        "\t\t\t\t\t\tand ft.code <> 'dmo'\n" +
        "\t\t\t\t\n" +
        "      and LENGTH(LEFT(f.hfrcode::text, 6))::INT = 6")
    List<ZoneDto> getZones();
/*
    @Select("\n" +
        "SELECT hfrcode as \"orgUnit\", period, datasetid as \"dataElement\", \n" +
        "SUM(overstock)+SUM(SP)+sum(unknown)+ sum(understock)+SUM(SO) as \"value\"\n" +
        "FROM (\n" +
        "select \n" +
        "hfrcode ,\n" +
        "\n" +
        "(date_part('YEAR'::text, pp.startdate) || ''::text) || to_char(pp.enddate, 'MM'::text) AS period,\n" +
        "se.datasetid ,\n" +
        "\n" +
        "\n" +
        "CASE WHEN Status ='SP' then 1 else 0 END as SP,\n" +
        "CASE WHEN Status ='UK' then 1 ELSE 0 END as unknown,\n" +
        "CASE WHEN Status ='US' then 1 else 0 end as understock,\n" +
        "\n" +
        "CASE WHEN Status ='OS' then 1 ELSE 0 end as overstock,\n" +
        "CASE WHEN Status ='SO' then 1 ELSE 0 end as SO \n" +
        "\n" +
        "from mv_stock_imbalance_by_facility_report r \n" +
        "JOIN interface_dataset se ON r.productcode::text = se.datasetname::text and interfaceid = \n" +
        "(select id from interface_apps where name = 'NumberofExpected' )\n" +
        "JOIN processing_periods pp ON r.periodid = pp.id\n" +
        "JOIN facilities f ON r.facility_id = F.id\n" +
            "\t\t\t\t\t\t   join hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n"+
            " where programId = #{programId} \n" +
        "\n" +
        " and EMERGENCY = FALSE AND hfrcode IS NOT NULL and hfrcode not in('-', '.')\n" +
        " AND r.periodId = #{periodId}::INT\n" +
            " \t\t\t\t\t\t\t AND  hrf.operatingstatus = 'Operating' \n" +
            " AND STATUS NOT IN ('') \n" +
        " )l\n" +
        "GROUP BY hfrcode, period, l.datasetid\n" +
        "order by period,hfrcode ")*/

    @Select(" SELECT datasetid as dataElement, \n" +
        "            SUM(overstock)+SUM(SP)+sum(unknown)+ sum(understock) as value\n" +
        "            FROM (\n" +
        "            select \n" +
        "           \n" +
        "            se.datasetid ,\n" +
        "            \n" +
        "            CASE WHEN Status ='SP' then 1 else 0 END as SP,\n" +
        "            CASE WHEN Status ='UK' then 1 ELSE 0 END as unknown,\n" +
        "            CASE WHEN Status ='US' then 1 else 0 end as understock,\n" +
        "            \n" +
        "            CASE WHEN Status ='OS' then 1 ELSE 0 end as overstock,\n" +
        "            CASE WHEN Status ='SO' then 1 ELSE 0 end as SO \n" +
        "            \n" +
        "            from mv_stock_imbalance_by_facility_report r \n" +
        "            JOIN interface_dataset se ON r.productcode::text = se.datasetname::text \n" +
        "\t\t\tand interfaceid = (select id from interface_apps where name = 'NumberofStock' )\n" +
        "            where  \n" +
        "              r.programID IN (7,8,1,2,3,10) and periodid = #{periodId}\n" +
        "\t\t\t\tand facility_Id = #{facilityId}\n" +
        "             AND STATUS NOT IN ('')  and se.datasetid not in('xQbX2k9UFq6')\n" +
        "             )l\n" +
        "            GROUP BY l.datasetid\n" +
        "\t\t\t")
    List<DataSetElement> getNumberOfExpected(@Param("periodId") Long periodId, @Param("facilityId") Long facilityId);

    @Select(" select * from programs where active = true")
    List<ProgramDto> getAllPrograms2();

    @Select("Select code as programCode, name, description, uuid_generate_v4() uuid from programs limit #{limit} offset #{offset} ")
    List<Program> programs(@Param("L1") long l, long l1,
                                            long l2,@Param("limit") int pageSize,
                                            @Param("offset") int offSet);

    @Select("SELECT * FROM products where active = true ")
    List<ProductTest> getAllP();

    @Insert("INSERT INTO public.hfr_facilities(\n" +
            "             commfacname, council, createdat, district, facidnumber, facilitytype, \n" +
            "            facilitytypegroup, latitude, longitude, name, oschangeclosedtooperational, \n" +
            "            oschangeopenedtoclose, operatingstatus, ownership, ownershipgroup, \n" +
            "            postorupdate, region, registrationstatus, updatedat, villagemtaa, \n" +
            "            ward, zone, districtCode, councilCode,facilityTypeGroupCode,ownershipCode)\n" +
            "    VALUES ( #{commFacName}, #{council}, #{createdAt}, #{district}, #{facIDNumber}, #{facilityType}, \n" +
            "            #{facilityTypeGroup},#{latitude}, #{longitude}, #{name}, #{oSchangeClosedtoOperational}, \n" +
            "            #{oSchangeOpenedtoClose}, #{operatingStatus}, #{ownership}, #{ownershipGroup}, \n" +
            "            #{postorUpdate}, #{region}, #{registrationStatus}, #{updatedAt}, #{villageMtaa}, \n" +
            "            #{ward}, #{zone}, #{districtCode}, #{councilCode}, #{facilityTypeGroupCode}, #{ownershipCode}); ")
    @Options(useGeneratedKeys = true)
    Integer insert(HealthFacilityDTO dto);

    @Update("UPDATE public.hfr_facilities\n" +
            "   SET  commfacname=#{commFacName}, council=#{council}, createdat=#{createdAt}, district=#{district}, \n" +
            "       facilitytype=#{facilityType}, facilitytypegroup= #{facilityTypeGroup}, latitude=#{latitude}, longitude=#{longitude}, \n" +
            "       name=#{name}, oschangeclosedtooperational=#{oSchangeClosedtoOperational}, oschangeopenedtoclose=#{oSchangeOpenedtoClose}, \n" +
            "       operatingstatus=#{operatingStatus}, ownership=#{ownership}, ownershipgroup=#{ownershipGroup}, postorupdate=#{postorUpdate}, \n" +
            "       region=#{region}, registrationstatus= #{registrationStatus}, updatedat=#{updatedAt}, villagemtaa=#{villageMtaa}, ward=#{ward}, \n" +
            "       zone=#{zone}, districtCode = #{districtCode},councilCode = #{councilCode}, facilityTypeGroupCode=#{facilityTypeGroupCode}, ownershipCode=#{ownershipCode}  \n" +
            " WHERE facidNumber=#{facIDNumber} ;")
    void update(HealthFacilityDTO dto);

    @Select("select * from hfr_facilities where facIDNumber = #{facIDNumber} limit 1")
    HealthFacilityDTO getByFacilityCode(@Param("facIDNumber") String facIDNumber);

    @Select("\n" +
            "SELECT hfrcode as \"orgUnit\", period, datasetid as \"dataElement\", \n" +
            "SUM(overstock)+SUM(SP)+sum(unknown)+ sum(understock)+SUM(SO) as \"value\"\n" +
            "FROM (\n" +
            "select \n" +
            "hfrcode ,\n" +
            "\n" +
            "(date_part('YEAR'::text, pp.startdate) || ''::text) || to_char(pp.enddate, 'MM'::text) AS period,\n" +
            "se.datasetid ,\n" +
            "\n" +
            "\n" +
            "CASE WHEN Status ='SP' then 1 else 0 END as SP,\n" +
            "CASE WHEN Status ='UK' then 1 ELSE 0 END as unknown,\n" +
            "CASE WHEN Status ='US' then 1 else 0 end as understock,\n" +
            "\n" +
            "CASE WHEN Status ='OS' then 1 ELSE 0 end as overstock,\n" +
            "CASE WHEN Status ='SO' then 1 ELSE 0 end as SO \n" +
            "\n" +
            "from mv_stock_imbalance_by_facility_report r \n" +
            "JOIN interface_dataset se ON r.productcode::text = se.datasetname::text and interfaceid = \n" +
            "(select id from interface_apps where name = 'NumberofExpected' )\n" +
            "JOIN processing_periods pp ON r.periodid = pp.id\n" +
            "JOIN facilities f ON r.facility_id = F.id\n" +
             " LEFT join hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n"+

            " where " +
            "  r.programID IN (7,8,1,2,3,10) and pp.id = #{periodId} and r.facilityId = #{zoneId} " +
           // " \t\t\t\t\t\t\t AND  hrf.operatingstatus = 'Operating' \n" +
            "\n" +
            " and EMERGENCY = FALSE and f.active = true and f.enabled = true\n" +
            " AND STATUS NOT IN ('')  and se.datasetid not in('xQbX2k9UFq6')\n" +
            " )l\n" +
            "GROUP BY hfrcode, period, l.datasetid\n" +
            "order by period,hfrcode ")
    List<ElmisInterfaceDataSetDto> getDhisNumberOfExpected(@Param("periodId") Long periodId,
                                                           @Param("zoneId") Long zoneId);

   /* @Select("SELECT hfrcode as \"orgUnit\", period, datasetid as \"dataElement\", \n" +
            "\n" +
            "\n" +
            "\n" +
            "SUM(overstock)+SUM(SP)+ sum(understock) as \"value\"\n" +
            "\n" +
            "\n" +
            "FROM (\n" +
            "select \n" +
            "hfrcode ,\n" +
            "\n" +
            "(date_part('YEAR'::text, pp.startdate) || ''::text) || to_char(pp.enddate, 'MM'::text) AS period,\n" +
            "se.datasetid ,\n" +
            "\n" +
            "\n" +
            "CASE WHEN Status ='SP' then 1 else 0 END as SP,\n" +
            "CASE WHEN Status ='UK' then 1 ELSE 0 END as unknown,\n" +
            "CASE WHEN Status ='US' then 1 else 0 end as understock,\n" +
            "\n" +
            "CASE WHEN Status ='OS' then 1 ELSE 0 end as overstock,\n" +
            "CASE WHEN Status ='SO' then 1 ELSE 0 end as SO \n" +
            "\n" +
            "from mv_stock_imbalance_by_facility_report r \n" +
            "JOIN interface_dataset se ON r.productcode::text = se.datasetname::text and interfaceid = 7\n" +
            "JOIN processing_periods pp ON r.periodid = pp.id\n" +
            "JOIN facilities f ON r.facility_id = F.id\n" +
            "\t\t\t\t\t\t  LEFT JOIN hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n"+

            "where " +
            "  r.programID IN (7,8,1,2,3,10) and pp.id = #{periodId} and zone_id = #{zoneId} " +
         //   " \t\t\t\t\t\t\t AND  hrf.operatingstatus = 'Operating' \n" +
         //   " and EMERGENCY = FALSE  AND hfrcode IS NOT NULL and hfrcode not in('-', '.') \n" +
        " and EMERGENCY = FALSE AND f.hfrcode::text ~ '^\\d{6}'::text and f.active = true and f.enabled = true\n" +

        "AND STATUS NOT IN ('')  and se.datasetid not in('xQbX2k9UFq6')\n" +
            "\n" +
            ")l\n" +
            "GROUP BY hfrcode, period, l.datasetid\n" +
            "order by period,hfrcode")*/
  /* @Select(" SELECT datasetid as dataElement, \n" +
       "            SUM(overstock)+SUM(SP)+sum(unknown)+ sum(understock) + sum(SO) as value\n" +
       "            FROM (\n" +
       "            select \n" +
       "           \n" +
       "            se.datasetid ,\n" +
       "            \n" +
       "            CASE WHEN Status ='SP' then 1 else 0 END as SP,\n" +
       "            CASE WHEN Status ='UK' then 1 ELSE 0 END as unknown,\n" +
       "            CASE WHEN Status ='US' then 1 else 0 end as understock,\n" +
       "            \n" +
       "            CASE WHEN Status ='OS' then 1 ELSE 0 end as overstock,\n" +
       "            CASE WHEN Status ='SO' then 1 ELSE 0 end as SO \n" +
       "            \n" +
       "            from mv_stock_imbalance_by_facility_report r \n" +
       "            JOIN interface_dataset se ON r.productcode::text = se.datasetname::text \n" +
       "\t\t\tand interfaceid = (select id from interface_apps where name = 'NumberofExpected' )\n" +
       "            where  \n" +
       "              r.programID IN (7,8,1,2,3,10) and periodid = #{periodId}\n" +
       "\t\t\t\tand facility_Id = #{facilityId}\n" +
       "             AND STATUS NOT IN ('')  and se.datasetid not in('xQbX2k9UFq6')\n" +
       "             )l\n" +
       "            GROUP BY l.datasetid\n" +
       "\t\t\t")*/

   @Select("SELECT datasetid as dataElement, \n" +
       "            SUM(overstock)+SUM(SP)+sum(unknown)+ sum(understock) + sum(SO) as value\n" +
       "            FROM (\n" +
       "            select \n" +
       "           \n" +
       "            se.datasetid ,\n" +
       "            \n" +
       "            CASE WHEN Status ='SP' then 1 else 0 END as SP,\n" +
       "            CASE WHEN Status ='UK' then 1 ELSE 0 END as unknown,\n" +
       "            CASE WHEN Status ='US' then 1 else 0 end as understock,\n" +
       "            \n" +
       "            CASE WHEN Status ='OS' then 1 ELSE 0 end as overstock,\n" +
       "            CASE WHEN Status ='SO' then 1 ELSE 0 end as SO \n" +
       "            \n" +
       "            from mv_stock_imbalance_by_facility_report r \n" +
       "            JOIN interface_dataset se ON r.productcode::text = se.datasetname::text \n" +
       "\t\t\tand interfaceid = (select id from interface_apps where name = 'NumberofExpected' )\n" +
       "            where  \n" +
       "              r.programID IN (7,8,1,2,3,10) and periodid = #{periodId}\n" +
       "\t\t\t\tand facility_Id = #{facilityId}\n" +
       "             AND STATUS NOT IN ('')  and se.datasetid not in('xQbX2k9UFq6')\n" +
       "             )l\n" +
       "            GROUP BY l.datasetid")
    List<DataSetElement> getAvailability(@Param("periodId") Long periodId, @Param("facilityId") Long facilityId);

    @Select( "select se.datasetid dataElement,\n" +
        "           sum(stockinhand) as value\n" +
        "           from requisitions r\n" +
        "           JOIN requisition_line_items item ON r.id = item.rnrid\n" +
        "           JOIN facilities f ON r.facilityId = F.id\n" +
        "           JOIN processing_periods pp ON r.periodid = pp.id\n" +
        "           JOIN programs pr ON r.programId = PR.ID\n" +
        "           JOIN interface_dataset se ON item.productcode::text = se.datasetname::text and interfaceid = 1\n" +
        "           LEFT join hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n" +
        "\n" +
        "           where \n" +
        "            pr.id IN (7,8,1,2,3,10) \n" +
        "\t\t    and  \n" +
        "\t\t\tpp.id = #{periodId} and facilityId = #{facilityId}\n" +
        "            and stockinhand > 0\n" +
        "\n" +
        "        and R.EMERGENCY = FALSE \n" +
        "\t\tand f.active = true and f.enabled = true\n" +
        "\n" +
        "       --AND  hrf.operatingstatus = 'Operating'\n" +
        "            and se.datasetid not in('xQbX2k9UFq6')\n" +
        "           group by\n" +
        "            datasetid")
    List<DataSetElement> getMonthly(@Param("periodId") Long id, @Param("scheduleId") Long schedule,
                                              @Param("facilityId") Long facilityId);

    @Select("select se.datasetid  dataElement,\n" +
        "            sum(quantityapproved) as value\n" +
        "            from requisitions r\n" +
        "            JOIN requisition_line_items item ON r.id = item.rnrid\n" +
        "            JOIN facilities f ON r.facilityId = F.id\n" +
        "            JOIN vw_districts d on f.geographiczoneid = d.district_id\n" +
        "            JOIN processing_periods pp ON r.periodid = pp.id\n" +
        "            JOIN programs pr ON r.programId = PR.ID\n" +
        "            JOIN interface_dataset se ON item.productcode::text = se.datasetname::text and interfaceid = 3\n" +
        "             LEFT join hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n" +
        "            where\n" +
        "              \n" +
        "             pr.id IN (7,8,1,2,3,10) and  pp.id = #{periodId} and r.facilityid = #{facilityId}\n" +
        "             and quantityapproved > 0\n" +
        "           --AND  hrf.operatingstatus = 'Operating' \n" +
        "            and EMERGENCY = FALSE and f.active = true and f.enabled = true\n" +
        "\n" +
        "         and se.datasetid not in('xQbX2k9UFq6') \n" +
        "            group by\n" +
        "             datasetid")
    List<DataSetElement> getOrdered(@Param("periodId") Long id, @Param("scheduleId") Long schedule,
                                              @Param("facilityId") Long facilityId);

    @Select("\n" +
        "\t\tselect se.datasetid  dataElement,\n" +
        "            sum(quantityreceived) as value\n" +
        "            from requisitions r\n" +
        "            JOIN requisition_line_items item ON r.id = item.rnrid\n" +
        "            JOIN facilities f ON r.facilityId = F.id\n" +
        "            JOIN processing_periods pp ON r.periodid = pp.id\n" +
        "            JOIN programs pr ON r.programId = PR.ID\n" +
        "            JOIN interface_dataset se ON item.productcode::text = se.datasetname::text and interfaceid = 2\n" +
        "            LEFT join hfr_facilities hrf on f.hfrcode = hrf.facidnumber\n" +
        "            where\n" +
        "             pr.id IN (7,8,1,2,3,10) and pp.id = #{periodId} and r.facilityId = #{facilityId} \n" +
        "             and quantityreceived > 0\n" +
        "\n" +
        "         and EMERGENCY = FALSE and f.active = true and f.enabled = true\n" +
        "\n" +
        "         and se.datasetid not in('xQbX2k9UFq6') \n" +
        "            group by\n" +
        "            datasetid\n" +
        "\t\t\t")
    List<DataSetElement> getQuantityReceived(@Param("periodId") Long id, @Param("scheduleId") Long schedule,
                                                       @Param("facilityId") Long facilityId);
/*
    @Select(" SELECT * FROM vw_bed_nets_data_main_land where period<=to_char(now(), 'YYYYMM')::int " +
        " AND  period>=202109::int and reporting_year in(2021,2022) and value > 0 order by period desc ")*/
/*   @Select("\n" +
       "\t SELECT * FROM vw_bed_nets_data_main_land where period<=to_char(now(), 'YYYYMM')::int \n" +
       "     AND  period>=202112::int and reporting_year in(2022) and value > 0 order by period desc")*/

/*   @Select(" SELECT * FROM vw_bed_nets_data_main_land where period<=to_char(now(), 'YYYYMM')::int \n" +
       "            AND  period>=202111::int and period < 202205 and reporting_year in(2022) and value > 0 order by period desc")*/
   @Select("SELECT * FROM vw_bed_nets_data_main_land where period<=to_char(now(), 'YYYYMM')::int \n" +
       "                 AND  period>=202204::int and period < 202206 and reporting_year in(2022) and value > 0 order by period desc")
    List<ElmisInterfaceDataSetDto> getMosquitoNetData(@Param("year") Long year);

    @Insert("INSERT INTO integration.logs(\n" +
        "\t jsonString, sent)\n" +
        "\tVALUES ( #{jsonString},  #{sent});")
  @Options(useGeneratedKeys = true)
   Integer insertLog(Log log);

    @Insert("INSERT INTO integration.log_responses(\n" +
        "\tjsonString, errorString, createdDate)\n" +
        "\tVALUES ( #{jsonString}, #{errorString}, NOW());")
  void insertResponse(LogResponse response);

    @Delete(" Delete from integration.logs WHERE id = #{id}")
   void delete(@Param("id") Long id);

  @Select("SELECT id, jsonString FROM integration.logs")
  List<Log> getLogs();

  @Select("SELECT id, jsonString FROM integration.logs order by id desc LIMIT 1 ")
  Log getOnLog();
}
